/*  Copyright (C) 2022-2023 libreki
 *
 *  This file is part of Anirater.
 *
 *  Anirater is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Anirater is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Anirater.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "anirater-subcategory.h"

struct _AniraterSubcategory
{
  GtkBox     parent;

  char      *name;
  GtkWidget *name_label;
  char      *description;
  GtkWidget *description_label;

  /* GtkButton box. Stores the same data as the vote_buttons array. */
  GtkWidget *vote_box;

  /* Stores GtkButtons. Indices indicate the numeric value of the vote. Each
   * button has a GObject association 'vote_value' to store the numeric value
   * of the vote. */
  GPtrArray *vote_buttons;

  /* Vote cast by the user for this subcategory. */
  guint      vote;
};

G_DEFINE_TYPE (AniraterSubcategory, anirater_subcategory, GTK_TYPE_BOX);

/* Callback function for when a vote button is pressed. */
static void
cast_vote (GtkButton           *vote_button,
           AniraterSubcategory *subcategory)
{
  subcategory->vote = *(guint *) g_object_get_data (G_OBJECT (vote_button), "vote_value");
}

typedef enum
{
  PROP_NAME = 1,
  PROP_DESCRIPTION,
  PROP_VOTE_BUTTONS,
  N_PROPERTIES
} AniraterSubcategoryProperty;

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL };

static void
anirater_subcategory_set_property (GObject      *object,
                                   guint         property_id,
                                   const GValue *value,
                                   GParamSpec   *pspec)
{
  AniraterSubcategory *self = ANIRATER_SUBCATEGORY (object);

  switch ((AniraterSubcategoryProperty) property_id)
    {
    case PROP_NAME:
      self->name = g_value_dup_string (value);
      break;

    case PROP_DESCRIPTION:
      self->description = g_value_dup_string (value);
      break;

    case PROP_VOTE_BUTTONS:
      self->vote_buttons = g_value_get_pointer (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
anirater_subcategory_finalize (GObject *object)
{
  AniraterSubcategory *self = ANIRATER_SUBCATEGORY (object);

  g_free (self->name);
  g_free (self->description);
  g_ptr_array_free (self->vote_buttons, TRUE);

  G_OBJECT_CLASS (anirater_subcategory_parent_class)->finalize (object);
}

static void
anirater_subcategory_constructed (GObject *object)
{
  AniraterSubcategory *self = ANIRATER_SUBCATEGORY (object);

  /* Set the name and description labels to match the 'name' and 'description'
   * construct properties. */
  gtk_label_set_text (GTK_LABEL (self->name_label), self->name);
  gtk_label_set_text (GTK_LABEL (self->description_label),
                      self->description);

  /* Add the vote buttons from the 'vote_buttons' construct property to the
   * vote box, so they are visible in the UI. */
  for (guint n = 0; n < self->vote_buttons->len; n++)
    {
      GtkWidget *vote_button;

      vote_button = g_ptr_array_index (self->vote_buttons, n);

      g_signal_connect (GTK_BUTTON (vote_button), "clicked",
                        G_CALLBACK (cast_vote), self);

      gtk_box_prepend (GTK_BOX (self->vote_box), vote_button);
    }
}

static void
anirater_subcategory_init (AniraterSubcategory *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->vote = 0;
}

static void
anirater_subcategory_class_init (AniraterSubcategoryClass *class)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);
  GObjectClass *object_class = G_OBJECT_CLASS (class);

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/codeberg/libreki/anirater/anirater-subcategory.ui");

  gtk_widget_class_bind_template_child (widget_class, AniraterSubcategory, name_label);
  gtk_widget_class_bind_template_child (widget_class, AniraterSubcategory, description_label);
  gtk_widget_class_bind_template_child (widget_class, AniraterSubcategory, vote_box);

  object_class->set_property = anirater_subcategory_set_property;
  object_class->constructed = anirater_subcategory_constructed;
  object_class->finalize = anirater_subcategory_finalize;

  obj_properties[PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "",
                         NULL,
                         G_PARAM_CONSTRUCT_ONLY | G_PARAM_WRITABLE);

  obj_properties[PROP_DESCRIPTION] =
    g_param_spec_string ("description",
                         "Description",
                         "",
                         NULL,
                         G_PARAM_CONSTRUCT_ONLY | G_PARAM_WRITABLE);

  obj_properties[PROP_VOTE_BUTTONS] =
    g_param_spec_pointer ("vote_buttons",
                          "Vote buttons",
                          "",
                          G_PARAM_CONSTRUCT_ONLY | G_PARAM_WRITABLE);

  g_object_class_install_properties (object_class, N_PROPERTIES, obj_properties);
}

AniraterSubcategory *
anirater_subcategory_new (const char *name,
                          const char *description,
                          GPtrArray  *vote_buttons)
{
  return g_object_new (ANIRATER_SUBCATEGORY_TYPE,
                       "name", name,
                       "description", description,
                       "vote_buttons", vote_buttons,
                       NULL);
}

guint
anirater_subcategory_get_vote (AniraterSubcategory *self)
{
  return self->vote;
}
