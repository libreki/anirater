/*  Copyright (C) 2022-2023 libreki
 *
 *  This file is part of Anirater.
 *
 *  Anirater is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Anirater is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Anirater.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "anirater-category.h"

struct _AniraterCategory
{
  GtkBox     parent;

  char      *name;
  GtkWidget *name_label;

  /* AniraterSubcategory stack. Stores the same data as the subcategories
   * array. */
  GtkWidget *stack;

  /* Stores AniraterSubcategory pointers. Indices indicate order. */
  GPtrArray *subcategories;
};

G_DEFINE_TYPE (AniraterCategory, anirater_category, GTK_TYPE_BOX);

typedef enum
{
  PROP_NAME = 1,
  PROP_SUBCATEGORIES,
  N_PROPERTIES
} AniraterCategoryProperty;

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL };

static void
anirater_category_set_property (GObject      *object,
                                guint         property_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  AniraterCategory *self = ANIRATER_CATEGORY (object);

  switch ((AniraterCategoryProperty) property_id)
    {
    case PROP_NAME:
      self->name = g_value_dup_string (value);
      break;

    case PROP_SUBCATEGORIES:
      self->subcategories = g_value_get_pointer (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
anirater_category_finalize (GObject *object)
{
  AniraterCategory *self = ANIRATER_CATEGORY (object);

  g_free (self->name);
  g_ptr_array_free (self->subcategories, TRUE);

  G_OBJECT_CLASS (anirater_category_parent_class)->finalize (object);
}

static void
anirater_category_constructed (GObject *object)
{
  AniraterCategory *self = ANIRATER_CATEGORY (object);

  /* Set the name label to match the 'name' construct property. */
  gtk_label_set_text (GTK_LABEL (self->name_label), self->name);

  /* Add the AniraterSubcategories from the 'subcategories' construct property
   * to the subcategory stack, so they can be shown later. */
  for (guint i = 0; i < self->subcategories->len; i++)
    {
      gtk_stack_add_child (GTK_STACK (self->stack),
                           g_ptr_array_index (self->subcategories, i));
    }
}

static void
anirater_category_init (AniraterCategory *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

static void
anirater_category_class_init (AniraterCategoryClass *class)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);
  GObjectClass *object_class = G_OBJECT_CLASS (class);

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/codeberg/libreki/anirater/anirater-category.ui");

  gtk_widget_class_bind_template_child (widget_class, AniraterCategory, name_label);
  gtk_widget_class_bind_template_child (widget_class, AniraterCategory, stack);

  object_class->set_property = anirater_category_set_property;
  object_class->constructed = anirater_category_constructed;
  object_class->finalize = anirater_category_finalize;

  obj_properties[PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "",
                         NULL,
                         G_PARAM_CONSTRUCT_ONLY | G_PARAM_WRITABLE);

  obj_properties[PROP_SUBCATEGORIES] =
    g_param_spec_pointer ("subcategories",
                          "Subcategories",
                          "",
                          G_PARAM_CONSTRUCT_ONLY | G_PARAM_WRITABLE);

  g_object_class_install_properties (object_class, N_PROPERTIES, obj_properties);
}

AniraterCategory *
anirater_category_new (const char *name,
                       GPtrArray  *subcategories)
{
  return g_object_new (ANIRATER_CATEGORY_TYPE,
                       "name", name,
                       "subcategories", subcategories,
                       NULL);
}

void
anirater_category_set_visible_subcategory (AniraterCategory *self,
                                           guint             i)
{
  gtk_stack_set_visible_child (GTK_STACK (self->stack),
                               g_ptr_array_index (self->subcategories, i));
}

AniraterSubcategory *
anirater_category_get_subcategory (AniraterCategory *self,
                                   guint             i)
{
  return ANIRATER_SUBCATEGORY (g_ptr_array_index (self->subcategories, i));
}

guint
anirater_category_get_subcategories_length (AniraterCategory *self)
{
  return self->subcategories->len;
}

const char *
anirater_category_get_name (AniraterCategory *self)
{
  return self->name;
}

/* The rating of a category is the sum of all subcategory votes. */
guint
anirater_category_get_rating (AniraterCategory *self)
{
  guint rating;

  rating = 0;
  for (guint i = 0; i < self->subcategories->len; i++)
    {
      AniraterSubcategory *subcategory;

      subcategory = g_ptr_array_index (self->subcategories, i);
      rating += anirater_subcategory_get_vote (subcategory);
    }

  return rating;
}
