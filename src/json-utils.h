/*  Copyright (C) 2022-2023 libreki
 *
 *  This file is part of Anirater.
 *
 *  Anirater is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Anirater is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Anirater.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef JSON_UTILS_H
#define JSON_UTILS_H

/* This file declares functions used to load data from categories.json. */

#include <json-c/json.h>

json_object       *json_load                        (void);
const json_object *json_get_categories              (const json_object *self);

const char        *json_category_get_name           (const json_object *self);
const json_object *json_category_get_subcategories  (const json_object *self);

const char        *json_subcategory_get_name        (const json_object *self);
const char        *json_subcategory_get_description (const json_object *self);
const json_object *json_subcategory_get_votes       (const json_object *self);

const char        *json_votes_get_vote              (const json_object *self,
                                                     int                n);

#endif
