/*  Copyright (C) 2022-2023 libreki
 *
 *  This file is part of Anirater.
 *
 *  Anirater is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Anirater is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Anirater.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "anirater-results-row.h"

struct _AniraterResultsRow
{
  GtkBox     parent;

  char      *name;
  GtkWidget *name_label;
  GtkWidget *rating_spin_button;
};

G_DEFINE_TYPE (AniraterResultsRow, anirater_results_row, GTK_TYPE_LIST_BOX_ROW);

typedef enum
{
  PROP_NAME = 1,
  N_PROPERTIES
} AniraterResultsRowProperty;

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL };

static void
anirater_results_row_set_property (GObject      *object,
                                   guint         property_id,
                                   const GValue *value,
                                   GParamSpec   *pspec)
{
  AniraterResultsRow *self = ANIRATER_RESULTS_ROW (object);

  switch ((AniraterResultsRowProperty) property_id)
    {
    case PROP_NAME:
      self->name = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
anirater_results_row_finalize (GObject *object)
{
  AniraterResultsRow *self = ANIRATER_RESULTS_ROW (object);

  g_free (self->name);

  G_OBJECT_CLASS (anirater_results_row_parent_class)->finalize (object);
}

static void
anirater_results_row_constructed (GObject *object)
{
  AniraterResultsRow *self = ANIRATER_RESULTS_ROW (object);

  /* Set the name label to match the 'name' construct property. */
  gtk_label_set_label (GTK_LABEL (self->name_label), self->name);
}

static void
anirater_results_row_init (AniraterResultsRow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  gtk_spin_button_set_range (GTK_SPIN_BUTTON (self->rating_spin_button), 0, 10);
  gtk_spin_button_set_increments (GTK_SPIN_BUTTON (self->rating_spin_button), 1, 0);
}

static void
anirater_results_row_class_init (AniraterResultsRowClass *class)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);
  GObjectClass *object_class = G_OBJECT_CLASS (class);

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/codeberg/libreki/anirater/anirater-results-row.ui");

  gtk_widget_class_bind_template_child (widget_class, AniraterResultsRow, name_label);
  gtk_widget_class_bind_template_child (widget_class, AniraterResultsRow, rating_spin_button);

  object_class->set_property = anirater_results_row_set_property;
  object_class->constructed = anirater_results_row_constructed;
  object_class->finalize = anirater_results_row_finalize;

  obj_properties[PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "",
                         NULL,
                         G_PARAM_CONSTRUCT_ONLY | G_PARAM_WRITABLE);

  g_object_class_install_properties (object_class, N_PROPERTIES, obj_properties);
}

AniraterResultsRow *
anirater_results_row_new (const char *name)
{
  return g_object_new (ANIRATER_RESULTS_ROW_TYPE,
                       "name", name,
                       NULL);
}

void
anirater_results_row_set_rating (AniraterResultsRow *self,
                                 guint               n)
{
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (self->rating_spin_button), n);
}

guint
anirater_results_row_get_rating (AniraterResultsRow *self)
{
  return gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (self->rating_spin_button));
}

const char *
anirater_results_row_get_name (AniraterResultsRow *self)
{
  return self->name;
}

/* Used to bind the spin button to update the average in AniraterResults. */
GtkSpinButton *
anirater_results_row_get_rating_spin_button (AniraterResultsRow *self)
{
  return GTK_SPIN_BUTTON (self->rating_spin_button);
}
