/*  Copyright (C) 2023 libreki
 *
 *  This file is part of Anirater.
 *
 *  Anirater is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Anirater is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Anirater.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "anirater-plaintext-results.h"

struct _AniraterPlaintextResults
{
  AdwWindow      parent;

  char          *text;
  GtkTextBuffer *buffer;
};

G_DEFINE_TYPE (AniraterPlaintextResults, anirater_plaintext_results, ADW_TYPE_WINDOW);

typedef enum
{
  PROP_TEXT = 1,
  N_PROPERTIES
} AniraterPlaintextResultsProperty;

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL };

static void
anirater_plaintext_results_set_property (GObject      *object,
                                         guint         property_id,
                                         const GValue *value,
                                         GParamSpec   *pspec)
{
  AniraterPlaintextResults *self = ANIRATER_PLAINTEXT_RESULTS (object);

  switch ((AniraterPlaintextResultsProperty) property_id)
    {
    case PROP_TEXT:
      self->text = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
anirater_plaintext_results_finalize (GObject *object)
{
  AniraterPlaintextResults *self = ANIRATER_PLAINTEXT_RESULTS (object);

  g_free (self->text);

  G_OBJECT_CLASS (anirater_plaintext_results_parent_class)->finalize (object);
}

static void
anirater_plaintext_results_constructed (GObject *object)
{
  AniraterPlaintextResults *self = ANIRATER_PLAINTEXT_RESULTS (object);

  /* Fill the text buffer with the 'text' construct property. */
  gtk_text_buffer_set_text (self->buffer, self->text, -1);

  G_OBJECT_CLASS (anirater_plaintext_results_parent_class)->constructed (object);
}

static void
anirater_plaintext_results_init (AniraterPlaintextResults *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

static void
anirater_plaintext_results_class_init (AniraterPlaintextResultsClass *class)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);
  GObjectClass *object_class = G_OBJECT_CLASS (class);

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/codeberg/libreki/anirater/anirater-plaintext-results.ui");

  gtk_widget_class_bind_template_child (widget_class, AniraterPlaintextResults, buffer);

  object_class->set_property = anirater_plaintext_results_set_property;
  object_class->constructed = anirater_plaintext_results_constructed;
  object_class->finalize = anirater_plaintext_results_finalize;

  obj_properties[PROP_TEXT] =
    g_param_spec_string ("text",
                         "Text",
                         "",
                         NULL,
                         G_PARAM_CONSTRUCT_ONLY | G_PARAM_WRITABLE);

  g_object_class_install_properties (object_class, N_PROPERTIES, obj_properties);
}


AniraterPlaintextResults *
anirater_plaintext_results_new (const char *text)
{
  return g_object_new (ANIRATER_PLAINTEXT_RESULTS_TYPE,
                       "text", text,
                       NULL);
}
