/*  Copyright (C) 2022-2023 libreki
 *
 *  This file is part of Anirater.
 *
 *  Anirater is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Anirater is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Anirater.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef ANIRATER_WINDOW_H
#define ANIRATER_WINDOW_H

#include <gtk/gtk.h>

#include "anirater-application.h"

#define ANIRATER_WINDOW_TYPE (anirater_window_get_type ())
G_DECLARE_FINAL_TYPE (AniraterWindow, anirater_window, ANIRATER, WINDOW, GtkApplicationWindow)

AniraterWindow *anirater_window_new (AniraterApplication *app);

#endif
