/*  Copyright (C) 2022-2023 libreki
 *
 *  This file is part of Anirater.
 *
 *  Anirater is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Anirater is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Anirater.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "anirater-results.h"

#include "anirater-category.h"
#include "anirater-results-row.h"

struct _AniraterResults
{
  GtkBox     parent;

  /* AniraterResultsRow list box. Stores teh same data as the results_rows
   * array. */
  GtkWidget *category_list_box;

  /* Stores AniraterResultRows. */
  GPtrArray *results_rows;

  GtkWidget *average_label;

  GtkWidget *plaintext_results_button;
};

G_DEFINE_TYPE (AniraterResults, anirater_results, GTK_TYPE_BOX);

/* Helper function for updating the label showing the average rating. */
static void
update_average_label (AniraterResults *results,
                      gfloat           average)
{
  char *text;

  text = g_strdup_printf ("%0.2f", average / results->results_rows->len);

  gtk_label_set_text (GTK_LABEL (results->average_label), text);
  g_free (text);
}

/* Callback function for when a category rating is manually adjusted from an
 * AniraterResultsRow. */
static void
update_average_from_rows (AniraterResults *results)
{
  gfloat average;

  average = 0;
  for (guint i = 0; i < results->results_rows->len; i++)
    {
      AniraterResultsRow *result_row;

      result_row = g_ptr_array_index (results->results_rows, i);
      average += anirater_results_row_get_rating (result_row);
    }

  update_average_label (results, average);
}

typedef enum
{
  PROP_CATEGORY_RESULTS_ROWS = 1,
  N_PROPERTIES
} AniraterResultsProperty;

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL };

static void
anirater_results_set_property (GObject      *object,
                               guint         property_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  AniraterResults *self = ANIRATER_RESULTS (object);

  switch ((AniraterResultsProperty) property_id)
    {
    case PROP_CATEGORY_RESULTS_ROWS:
      self->results_rows = g_value_get_pointer (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
anirater_results_finalize (GObject *object)
{
  AniraterResults *self = ANIRATER_RESULTS (object);

  g_ptr_array_free (self->results_rows, TRUE);

  G_OBJECT_CLASS (anirater_results_parent_class)->finalize (object);
}

static void
anirater_results_constructed (GObject *object)
{
  AniraterResults *self = ANIRATER_RESULTS (object);

  /* Add the AniraterResultsRows from the 'results_rows' construct property
   * to the category list box, so ratings of categories can be shown to the
   * user. */
  for (guint i = 0; i < self->results_rows->len; i++)
    {
      AniraterResultsRow *results_row;
      GtkSpinButton *spin_button;

      results_row = g_ptr_array_index (self->results_rows, i);
      spin_button = anirater_results_row_get_rating_spin_button (results_row);

      g_signal_connect_swapped (spin_button, "value-changed",
                                G_CALLBACK (update_average_from_rows), self);
      gtk_list_box_append (GTK_LIST_BOX (self->category_list_box), GTK_WIDGET (results_row));
    }
}

static void
anirater_results_init (AniraterResults *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

static void
anirater_results_class_init (AniraterResultsClass *class)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);
  GObjectClass *object_class = G_OBJECT_CLASS (class);

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/codeberg/libreki/anirater/anirater-results.ui");

  gtk_widget_class_bind_template_child (widget_class, AniraterResults, category_list_box);
  gtk_widget_class_bind_template_child (widget_class, AniraterResults, average_label);
  gtk_widget_class_bind_template_child (widget_class, AniraterResults, plaintext_results_button);

  object_class->set_property = anirater_results_set_property;
  object_class->constructed = anirater_results_constructed;
  object_class->finalize = anirater_results_finalize;

  obj_properties[PROP_CATEGORY_RESULTS_ROWS] =
    g_param_spec_pointer ("results_rows",
                          "Results rows",
                          "",
                          G_PARAM_CONSTRUCT_ONLY | G_PARAM_WRITABLE);

  g_object_class_install_properties (object_class, N_PROPERTIES, obj_properties);
}

AniraterResults *
anirater_results_new (GPtrArray *results_rows)
{
  return g_object_new (ANIRATER_RESULTS_TYPE,
                       "results_rows", results_rows,
                       NULL);
}

/* Called when the user finishes voting on every subcategory and is shown the
 * results. */
void
anirater_results_update (AniraterResults *self,
                         GPtrArray       *categories)
{
  gfloat average;

  average = 0;
  for (guint i = 0; i < categories->len; i++)
    {
      AniraterResultsRow *category_result_row;
      AniraterCategory *category;
      guint rating;

      category_result_row = g_ptr_array_index (self->results_rows, i);
      category = g_ptr_array_index (categories, i);
      rating = anirater_category_get_rating (category);

      anirater_results_row_set_rating (category_result_row, rating);

      average += rating;
    }

  update_average_label (self, average);
}

char *
anirater_results_as_string (AniraterResults *results)
{
  GString *string;
  const char *average;
  char *average_line;

  string = g_string_new (NULL);

  for (guint i = 0; i < results->results_rows->len; i++)
    {
      AniraterResultsRow *results_row;
      char *line;

      results_row = g_ptr_array_index (results->results_rows, i);

      line = g_strdup_printf ("%s: %d/10\n",
                              anirater_results_row_get_name (results_row),
                              anirater_results_row_get_rating (results_row));

      g_string_append (string, line);
      g_free (line);
    }

  average = gtk_label_get_text (GTK_LABEL (results->average_label));
  average_line = g_strdup_printf ("\nAverage: %s\n", average);

  g_string_append (string, average_line);
  g_free (average_line);

  return g_string_free_and_steal (string);
}

/* Used to bind the button to show an AniraterPlaintextResults window in
 * AniraterWindow. */
GtkButton *
anirater_results_get_plaintext_results_button (AniraterResults *self)
{
  return GTK_BUTTON (self->plaintext_results_button);
}
