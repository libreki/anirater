/*  Copyright (C) 2022-2023 libreki
 *
 *  This file is part of Anirater.
 *
 *  Anirater is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Anirater is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Anirater.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "json-utils.h"

#include <stdio.h>

#include <gtk/gtk.h>

json_object *
json_load (void)
{
  GFile *file;
  GFileInputStream *input;
  int size;
  char *buffer;
  json_object *json;

  file = g_file_new_for_uri ("resource:///org/codeberg/libreki/anirater/anidb-categories.json");
  input = g_file_read (file, NULL, NULL);

  g_seekable_seek (G_SEEKABLE (input), 0, G_SEEK_END, NULL, NULL);
  size = g_seekable_tell (G_SEEKABLE (input));
  g_seekable_seek (G_SEEKABLE (input), 0, G_SEEK_SET, NULL, NULL);

  buffer = g_malloc (size);
  g_input_stream_read (G_INPUT_STREAM (input), buffer, size, NULL, NULL);
  buffer[size - 1] = '\0';

  json = json_tokener_parse (buffer);

  g_object_unref (file);
  g_object_unref (input);
  g_free (buffer);

  return json;
}

const json_object *
json_get_categories (const json_object *self)
{
  return json_object_object_get (self, "categories");
}

const char *
json_category_get_name (const json_object *self)
{
  return json_object_get_string (json_object_object_get (self, "name"));
}

const json_object *
json_category_get_subcategories (const json_object *self)
{
  return json_object_object_get (self, "subcategories");
}

const char *
json_subcategory_get_name (const json_object *self)
{
  return json_object_get_string (json_object_object_get (self, "name"));
}

const char *
json_subcategory_get_description (const json_object *self)
{
  return json_object_get_string (json_object_object_get (self, "description"));
}

const json_object *
json_subcategory_get_votes (const json_object *self)
{
  return json_object_object_get (self, "votes");
}

const char *
json_votes_get_vote (const json_object *self,
                     int                n)
{
  return json_object_get_string (json_object_array_get_idx (self, n));
}
