/*  Copyright (C) 2022-2023 libreki
 *
 *  This file is part of Anirater.
 *
 *  Anirater is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Anirater is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Anirater.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "anirater-window.h"

#include "anirater-category.h"
#include "anirater-plaintext-results.h"
#include "anirater-results.h"
#include "anirater-results-row.h"
#include "json-utils.h"

struct _AniraterWindow
{
  GtkApplicationWindow  parent;

  GtkWidget            *undo_button;
  GtkWidget            *restart_button;
  GtkWidget            *menu_button;

  GtkWidget            *stack;

  GtkWidget            *start;
  GtkWidget            *start_button;
  /* Stores AniraterCategories. Indices indicate order. */
  GPtrArray            *categories;
  AniraterResults      *results;

  guint                 current_category;
  guint                 current_subcategory;
};

G_DEFINE_TYPE (AniraterWindow, anirater_window, GTK_TYPE_APPLICATION_WINDOW);

/* Helper function for getting the current category being voted on. */
static AniraterCategory *
get_current_category (AniraterWindow *window)
{
  return g_ptr_array_index (window->categories, window->current_category);
}

/* Helper function to update the currently visible subcategory to match the
 * current_subcategory variable. */
static void
update_visible_subcategory (AniraterWindow *window)
{
  anirater_category_set_visible_subcategory (get_current_category (window),
                                             window->current_subcategory);
}

/* Helper function to update the currently visible category to match the
 * current_category variable. */
static void
update_visible_category (AniraterWindow *window)
{
  gtk_stack_set_visible_child (GTK_STACK (window->stack),
                               GTK_WIDGET (get_current_category (window)));
  update_visible_subcategory (window);
}

/* Callback function for when the start button is pressed. */
static void
anirater_start (AniraterWindow *window)
{
  window->current_category = 0;
  window->current_subcategory = 0;

  update_visible_category (window);
}

/* Callback function for when the restart button is pressed. */
static void
anirater_restart (AniraterWindow *window)
{
  gtk_stack_set_visible_child (GTK_STACK (window->stack), window->start);
}

/* Callback function for when the undo button is pressed. */
static void
anirater_undo (AniraterWindow *window)
{
  /* If the results are currently shown, there is no need to update
   * current_category or current_subcategory because they're already set to the
   * appropriate values. */
  if (ANIRATER_IS_RESULTS (gtk_stack_get_visible_child (GTK_STACK (window->stack))))
    {
      update_visible_category (window);
      return;
    }

  if (ANIRATER_IS_CATEGORY (gtk_stack_get_visible_child (GTK_STACK (window->stack))))
    {
      /* If current_subcategory is 0, then current_category needs to be
       * decremented and current_subcategory must be set to the final
       * subcategory of the decremented category. */
      if (window->current_subcategory <= 0)
        {
          AniraterCategory *category;

          /* Unless the first subcategory is the first category is being shown,
           * there is nothing to undo. */
          if (window->current_category <= 0)
            return;

          window->current_category--;
          category = get_current_category (window);

          window->current_subcategory =
            anirater_category_get_subcategories_length (category)- 1;

          update_visible_category (window);

          return;
        }

      window->current_subcategory--;
      update_visible_subcategory (window);
    }
}

/* Callback function for when a subcategory vote is cast to progress to the
 * next vote or to show the results. */
static void
anirater_continue (AniraterWindow *window)
{
  AniraterCategory *category;

  category = get_current_category (window);

  /* If the final subcategory of a category is currently shown, then
   * current_category needs to be incremented and current_subcategory needs to
   * be set to 0. */
  if (window->current_subcategory >= anirater_category_get_subcategories_length (category) - 1)
    {
      /* Unless the final subcategory of the final category is being shown, in
       * which case the results will be shown. */
      if (window->current_category >= window->categories->len - 1)
        {
          anirater_results_update (window->results, window->categories);
          gtk_stack_set_visible_child (GTK_STACK (window->stack),
                                       GTK_WIDGET (window->results));
          return;
        }

      window->current_category++;
      window->current_subcategory = 0;
      update_visible_category (window);
      return;
    }

  window->current_subcategory++;
  update_visible_subcategory (window);
}

/* Callback function for when the show plaintext results button in the results
 * page is pressed. */
static void
show_plaintext_results (AniraterWindow *window)
{
  AniraterPlaintextResults *results_window;
  char *results_string;

  results_string = anirater_results_as_string (window->results);
  results_window = anirater_plaintext_results_new (results_string);
  g_free (results_string);

  gtk_window_set_transient_for (GTK_WINDOW (results_window), GTK_WINDOW (window));
  gtk_window_present (GTK_WINDOW (results_window));
}

static void
anirater_window_finalize (GObject *object)
{
  AniraterWindow *self = ANIRATER_WINDOW (object);

  g_ptr_array_free (self->categories, TRUE);

  G_OBJECT_CLASS (anirater_window_parent_class)->finalize (object);
}

static void
anirater_window_init (AniraterWindow *self)
{
  GtkBuilder *menu_builder;
  GMenuModel *menu_model;
  json_object *json;
  const json_object *json_categories;
  GPtrArray *category_results_rows;

  gtk_widget_init_template (GTK_WIDGET (self));

  menu_builder = gtk_builder_new_from_resource ("/org/codeberg/libreki/anirater/anirater-menu.ui");
  menu_model = G_MENU_MODEL (gtk_builder_get_object (menu_builder, "menu"));
  gtk_menu_button_set_menu_model (GTK_MENU_BUTTON (self->menu_button), menu_model);
  g_object_unref (menu_builder);

  self->categories = g_ptr_array_new ();
  category_results_rows = g_ptr_array_new ();
  json = json_load ();
  json_categories = json_get_categories (json);
  for (guint i = 0; i < json_object_array_length (json_categories); i++)
    {
      const json_object *json_category;
      const json_object *json_subcategories;
      AniraterCategory *category;
      GPtrArray *subcategories;
      AniraterResultsRow *category_results_row;

      subcategories = g_ptr_array_new ();
      json_category = json_object_array_get_idx (json_categories, i);
      json_subcategories = json_category_get_subcategories (json_category);
      for (guint j = 0; j < json_object_array_length (json_subcategories); j++)
        {
          const json_object *json_subcategory;
          const json_object *json_vote_options;
          AniraterSubcategory *subcategory;
          GPtrArray *vote_buttons;

          vote_buttons = g_ptr_array_new ();
          json_subcategory = json_object_array_get_idx (json_subcategories, j);
          json_vote_options = json_subcategory_get_votes (json_subcategory);
          for (guint n = 0; n < json_object_array_length (json_vote_options); n++)
            {
              GtkWidget *vote_button;
              guint *vote_value;

              vote_button = gtk_button_new ();
              gtk_button_set_label (GTK_BUTTON (vote_button),
                                    json_votes_get_vote (json_vote_options, n));

              vote_value = malloc (sizeof (guint));
              *vote_value = n;
              g_object_set_data_full (G_OBJECT (vote_button), "vote_value",
                                      vote_value, free);

              g_signal_connect_data (vote_button, "clicked",
                                     G_CALLBACK (anirater_continue), self,
                                     NULL, G_CONNECT_SWAPPED | G_CONNECT_AFTER);
              g_ptr_array_add (vote_buttons, vote_button);
            }

          subcategory = anirater_subcategory_new (json_subcategory_get_name (json_subcategory),
                                                  json_subcategory_get_description (json_subcategory),
                                                  vote_buttons);
          g_ptr_array_add (subcategories, subcategory);
        }

      category = anirater_category_new (json_category_get_name (json_category), subcategories);
      g_ptr_array_add (self->categories, category);

      category_results_row = anirater_results_row_new (json_category_get_name (json_category));
      g_ptr_array_add (category_results_rows, category_results_row);

      gtk_stack_add_child (GTK_STACK (self->stack), GTK_WIDGET (category));
    }

  json_object_put (json);

  self->results = anirater_results_new (category_results_rows);
  g_signal_connect_swapped (anirater_results_get_plaintext_results_button (self->results),
                            "clicked", G_CALLBACK (show_plaintext_results), self);
  gtk_stack_add_child (GTK_STACK (self->stack), GTK_WIDGET (self->results));

  gtk_stack_set_visible_child (GTK_STACK (self->stack), self->start);
  gtk_window_set_child (GTK_WINDOW (self), GTK_WIDGET (self->stack));
}

static void
anirater_window_class_init (AniraterWindowClass *class)
{
  GtkWidgetClass *widget_class;
  GObjectClass *object_class;

  widget_class = GTK_WIDGET_CLASS (class);
  object_class = G_OBJECT_CLASS (class);

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/codeberg/libreki/anirater/anirater-window.ui");

  gtk_widget_class_bind_template_child (widget_class, AniraterWindow, undo_button);
  gtk_widget_class_bind_template_child (widget_class, AniraterWindow, restart_button);
  gtk_widget_class_bind_template_child (widget_class, AniraterWindow, menu_button);
  gtk_widget_class_bind_template_child (widget_class, AniraterWindow, stack);
  gtk_widget_class_bind_template_child (widget_class, AniraterWindow, start);
  gtk_widget_class_bind_template_child (widget_class, AniraterWindow, start_button);

  gtk_widget_class_bind_template_callback (widget_class, anirater_undo);
  gtk_widget_class_bind_template_callback (widget_class, anirater_restart);
  gtk_widget_class_bind_template_callback (widget_class, anirater_start);

  object_class->finalize = anirater_window_finalize;
}

AniraterWindow *
anirater_window_new (AniraterApplication *app)
{
  return g_object_new (ANIRATER_WINDOW_TYPE,
                       "application", app,
                       NULL);
}
