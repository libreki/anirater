/*  Copyright (C) 2022-2023 libreki
 *
 *  This file is part of Anirater.
 *
 *  Anirater is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Anirater is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Anirater.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef ANIRATER_RESULTS_ROW_H
#define ANIRATER_RESULTS_ROW_H

#include <gtk/gtk.h>

#define ANIRATER_RESULTS_ROW_TYPE (anirater_results_row_get_type ())
G_DECLARE_FINAL_TYPE (AniraterResultsRow, anirater_results_row, ANIRATER, RESULTS_ROW, GtkListBoxRow)

AniraterResultsRow *anirater_results_row_new                    (const char *name);

void                anirater_results_row_set_rating             (AniraterResultsRow *self,
                                                                 guint               n);

guint               anirater_results_row_get_rating             (AniraterResultsRow *self);
const char         *anirater_results_row_get_name               (AniraterResultsRow *self);
GtkSpinButton      *anirater_results_row_get_rating_spin_button (AniraterResultsRow *self);

#endif
