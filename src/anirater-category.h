/*  Copyright (C) 2022-2023 libreki
 *
 *  This file is part of Anirater.
 *
 *  Anirater is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Anirater is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Anirater.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef ANIRATER_CATEGORY_H
#define ANIRATER_CATEGORY_H

#include <gtk/gtk.h>

#include "anirater-subcategory.h"

#define ANIRATER_CATEGORY_TYPE (anirater_category_get_type ())
G_DECLARE_FINAL_TYPE (AniraterCategory, anirater_category, ANIRATER, CATEGORY, GtkBox)

AniraterCategory    *anirater_category_new                      (const char *name,
                                                                 GPtrArray  *subcategories);

void                 anirater_category_set_visible_subcategory  (AniraterCategory *self,
                                                                 guint             i);

AniraterSubcategory *anirater_category_get_subcategory          (AniraterCategory *self,
                                                                 guint             i);
guint                anirater_category_get_subcategories_length (AniraterCategory *self);
const char          *anirater_category_get_name                 (AniraterCategory *self);
guint                anirater_category_get_rating               (AniraterCategory *self);

#endif
