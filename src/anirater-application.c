/*  Copyright (C) 2022-2023 libreki
 *
 *  This file is part of Anirater.
 *
 *  Anirater is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Anirater is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Anirater.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "anirater-application.h"

#include "anirater-window.h"

struct _AniraterApplication
{
  AdwApplication  parent;

  AniraterWindow *window;
};

G_DEFINE_TYPE (AniraterApplication, anirater_application, ADW_TYPE_APPLICATION);

static void
action_about (GSimpleAction *action,
              GVariant      *parameter,
              gpointer       app)
{
  adw_show_about_window (GTK_WINDOW (ANIRATER_APPLICATION (app)->window),
                         "application-icon", "org.codeberg.libreki.anirater",
                         "application-name", "Anirater",
                         "website", "https://codeberg.org/libreki/anirater",
                         "copyright", "Copyright © 2022-2023 libreki",
                         "license-type", GTK_LICENSE_GPL_3_0,
                         NULL);
}

static void
action_quit (GSimpleAction *action,
             GVariant      *parameter,
             gpointer       app)
{
  g_application_quit (G_APPLICATION (app));
}

static GActionEntry app_entries[] =
{
  { "about", action_about, NULL, NULL, NULL },
  { "quit", action_quit, NULL, NULL, NULL }
};

static void
anirater_application_startup (GApplication *app)
{
  G_APPLICATION_CLASS (anirater_application_parent_class)->startup (app);

  g_action_map_add_action_entries (G_ACTION_MAP (app),
                                   app_entries, G_N_ELEMENTS (app_entries),
                                   app);
}

static void
anirater_application_activate (GApplication *app)
{
  AniraterApplication *self = ANIRATER_APPLICATION (app);

  self->window = anirater_window_new (self);
  gtk_window_present (GTK_WINDOW (self->window));
}

static void
anirater_application_init (AniraterApplication *self)
{
}

static void
anirater_application_class_init (AniraterApplicationClass *class)
{
  GApplicationClass *application_class = G_APPLICATION_CLASS (class);

  application_class->startup = anirater_application_startup;
  application_class->activate = anirater_application_activate;
}

AniraterApplication *
anirater_application_new (void)
{
  return g_object_new (ANIRATER_APPLICATION_TYPE,
                       "application-id", "org.codeberg.libreki.anirater",
                       "flags", G_APPLICATION_DEFAULT_FLAGS,
                       NULL);
}
