#  Copyright (C) 2022-2023 libreki
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  SPDX-License-Identifier: GPL-3.0-or-later

BIN = anirater

CFLAGS        = -std=c99 -g -Wall -Wextra -Wno-unused-parameter -Wno-missing-field-initializers \
                $(shell pkg-config --cflags gtk4) $(shell pkg-config --cflags libadwaita-1)
CPPFLAGS      = -c
LDLIBS        = $(shell pkg-config --libs gtk4) $(shell pkg-config --libs libadwaita-1) -ljson-c
VALGRINDFLAGS = --leak-check=full

SRCDIR = src
SRCS   = $(shell find $(SRCDIR) -name '*.c')

DATADIR   = data
GRESOURCE = $(DATADIR)/anirater.gresource.xml
RESOURCE  = $(DATADIR)/resource.c
UIS       = $(shell find $(DATADIR) -name '*.ui')

BUILDDIR = build
OBJDIR   = $(BUILDDIR)/obj
OBJS     = $(patsubst %.c, $(OBJDIR)/%.o, $(SRCS)) $(patsubst %.c, $(OBJDIR)/%.o, $(RESOURCE))

DESKTOP = org.codeberg.libreki.anirater.desktop
ICON    = org.codeberg.libreki.anirater.png

.PHONY: all run valgrind valgrindflag install uninstall clean
all: $(BUILDDIR)/$(BIN)

$(RESOURCE): $(GRESOURCE) $(UIS)
	glib-compile-resources $(GRESOURCE) --target=$(RESOURCE) --sourcedir=$(DATADIR) --generate-source

$(BUILDDIR)/$(BIN): $(OBJS)
	$(CC) $^ -o $@ $(LDLIBS)

$(OBJDIR)/%.o: %.c
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $(CPPFLAGS) $< -o $@

run: $(BUILDDIR)/$(BIN)
	$(BUILDDIR)/$(BIN)

valgrind: $(BUILDDIR)/$(BIN)
	valgrind $(BUILDDIR)/$(BIN)

valgrindflag: $(BUILDDIR)/$(BIN)
	valgrind $(VALGRINDFLAGS) $(BUILDDIR)/$(BIN)

install: $(BUILDDIR)/$(BIN)
	install -Dm755 $(BUILDDIR)/$(BIN) /usr/bin/$(BIN)
	install -Dm644 $(DATADIR)/$(ICON) /usr/share/icons/hicolor/128x128/apps/$(ICON)
	install -Dm644 $(DATADIR)/$(DESKTOP) /usr/share/applications/$(DESKTOP)
	gtk-update-icon-cache /usr/share/icons/hicolor

uninstall:
	rm -f /usr/bin/$(BIN)
	rm -f /usr/share/icons/hicolor/128x128/apps/$(ICON)
	rm -f /usr/share/applications/$(DESKTOP)

clean:
	rm -rf $(BUILDDIR) $(RESOURCE)
