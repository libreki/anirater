<!--SPDX-License-Identifier: FSFAP-->

# Anirater

> Copyright © 2021-2023 libreki
>
> Copying and distribution of this file, with or without modification,
> are permitted in any medium without royalty provided the copyright
> notice and this notice are preserved.  This file is offered as-is,
> without any warranty.

This is a simple program to aid in giving numerical ratings to any given object
of evaluation using a set of categories to rate it across.

Provided is a set of categories for rating anime with
[AniDB](https://anidb.net/)'s review categories.

## Building

### Libraries

- [GTK 4](https://www.gtk.org/)
- [Libadwaita](https://gnome.pages.gitlab.gnome.org/libadwaita/)
- [json-c](
  https://json-c.github.io/json-c/json-c-current-release/doc/html/index.html)

### Instructions

```
$ git clone https://codeberg.org/libreki/anirater.git
$ cd anirater
$ make
```

To run the program, use:

```
$ make run
```
